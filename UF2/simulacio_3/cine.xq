let $i := doc("cine.xml")//movie
return
<html>
  <head>
  </head>
  <body>
  <table align="center" border="1">
    <tr>
      <th>TITOL</th>
      <th>ANY</th>
    </tr>
    {
      for $movie in $i[contains(./type,"Science Fiction")]
        let $years := $movie/year
        order by $years
        return 
          <tr>
                <td>{string($movie/@title)}<br/></td>
                <td>{($movie/year)}<br/></td>
          </tr>
    }
    
    <tr>
      <td>Total de pelis</td>
      <td>{count($i[contains(./type,"Science Fiction")])}</td>
    </tr>
  </table>
  </body>
</html>