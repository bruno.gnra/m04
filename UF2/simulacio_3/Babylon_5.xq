let $i := doc("Babylon_5.xml")//babylon5
return 
<html>
  <head>
    <title>{data($i/dades_generals/titol)}</title>
    <link rel="stylesheet" type="text/css" href="css/estil_SOLUCIO.css"/>
  </head>
  <body>
  <table id="t01" align="center" border="1" width="900" >
    <tr>
      <th colspan="4">
        <a href="{$i/dades_generals/web}"><img src="logos/{$i/dades_generals/logo}" width="100%"></img></a>
      </th>
    </tr>
    <tr>
    { let $earth_alliance := sum($i//nau[origen = 'Earth_Alliance']/noms/@coneguts)
      let $narn := sum($i//nau[origen = 'Narn']/noms/@coneguts)
      return 
     <th colspan="4">
     Nº de noms de naus = {count($i//nom)}<br/>
     Nº de noms de naus coneguts = {sum($i//@coneguts)}<br/>
     Nº de noms de naus coneguts dorigen Earth_Alliance = {$earth_alliance}<br/>
    Nº de noms de naus coneguts dorigen Narn = {$narn}<br/>
     </th>
   }
    </tr>
    <tr>
      <th width="320">Nom</th>
      <th width="120">Bandol</th>
      <th width="320">Caracteristicas</th>
      <th width="140">Noms coneguts</th>
    </tr>
    {
     for $j in ($i/nau)
       order by ($j/origen)
     return 
    <tr>
      <td>
        <img src="imatges/{$j/imatge}" style="width: 100%;  height: auto;"></img><br/>
        <a href="">classe:{data($j/classe/@classe)}</a></td>
      <td>
        <img src="logos/{$j/logo}"></img>
      </td>
      <td>
        Tonelatge: {data($j/caracteristiques/tonelatge), string($j/caracteristiques/tonelatge/@unitat)}<br/>
        Logitud: {data($j/caracteristiques/longitud), string($j/caracteristiques/longitud/@unitat)}<br/>
        Tripulacio: {data($j/capacitat_humana/tripulacio)}<br/>
        Naus transportades:<br/>
        &#160;&#160;Caces: {data($j/capacitat_naus/caces)}<br/>
        &#160;&#160;Trasportes de tropes: {data($j/capacitat_naus/transports_tropes)}<br/>
        Abast:  {data($j/abast/quantitat), string($j/abast/unitat_de_mesura)}<br/>
        Propulsio:<br/>
        &#160;&#160;Reactors: {string($j/propulsio/reactors/@numero), data($j/propulsio/reactors)}<br/>
        &#160;&#160;Motors d impulió: {data($j/propulsio/numero_de_motors_impulsio), data($j/propulsio/motors_impulsio)}<br/>
        &#160;&#160;Potencia standar: {data($j/propulsio/potencia_standart), data($j/propulsio/unitat_de_mesura_potencia)}<br/>
        &#160;&#160;Potencia standar: {data($j/propulsio/potencia_militar), data($j/propulsio/unitat_de_mesura_potencia)}<br/>
      </td>
      <td>
        {$j/noms/nom}<br/>
        (Nº noms = {data(count($j/noms/nom))})
      </td>
    </tr>
    }

  </table>
  </body>
</html>
