<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match ="/">
    <html>
        <head>
            <title>Activitat 1 XSLT</title>
        </head>
        <body>
            <table border="1" align="center">
                <tr>
                    <th colspan="3" align="center"><b>Classificacio</b></th>
                </tr>
                <tr bgcolor="#00FF00" align="center">
                    <th>Plataforma del programa</th>
                    <th>Nom</th>
                    <th>Tipus de llicencia</th>
                </tr>
                <xsl:for-each select="programes/editors_XML/programa">
                    <xsl:sort select="llicencia"/>
                    <tr>
                        <td><xsl:value-of select="plataforma"/></td>
                        <td bgcolor="#FF0000" align="center"><b><xsl:value-of select="nom"/></b></td>
                        <td><xsl:value-of select="llicencia"/></td>
                    </tr> 
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>