<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <title>the Expanse</title>
                <link rel="stylesheet" href="css/estil_SOLUCIO.css" type="text/css"/>
            </head>
            <body>
                <table border="0" align="center">
                    <tr>
                        <!-- <th><xsl:value-of select="the_expanse/dades/titol"/></th> -->
                        <th>
                            <xsl:element name="img">
                                <xsl:attribute name="src">logos/<xsl:value-of select="the_expanse/dades/logo"></xsl:value-of></xsl:attribute>
                                <xsl:attribute name="alt"><xsl:value-of select="the_expanse/dades/titol"/></xsl:attribute>
                            </xsl:element>
                        </th>
                    </tr>
                </table>
                <table id="t01" border="1" align="center">
                    <tr>
                        <th width="320">Nom</th>
                        <th width="120">Bandol</th>
                        <th width="400">Característiques</th>
                        <th>Noms coneguts</th>
                    </tr>
                    <xsl:for-each select="the_expanse/Martian_Congressional_Republic_Navy/nau">
                        <tr class="normal">
                            <td align="center">
                                <img  width="320" height="150" >
                                    <xsl:attribute name="src">imatges/<xsl:value-of select="imatge"/></xsl:attribute>
                                    <xsl:attribute name="alt"><xsl:value-of select="the_expanse/dades/titol"/></xsl:attribute>
                                </img><br/>
                                <a target="_blank">
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="web"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="classe"/>
                                </a>
                            </td>
                            <td>
                                <img  width="120" >
                                    <xsl:attribute name="src">logos/<xsl:value-of select="logo"/></xsl:attribute>
                                    <xsl:attribute name="alt"><xsl:value-of select="the_expanse/dades/titol"/></xsl:attribute>
                                </img>
                            </td>
                            <td>

                            </td>
                            <!-- <td>
                                Tipus de nau: <xsl:value-of select="tipus"/><br/>
                                Tonelatge: <xsl:value-of select="caracteristiques/tonelatge"/> de <xsl:value-of select="caracteristiques/tonelatge/@unitat"/><br/>
                                Longitud: <xsl:value-of select="caracteristiques/longitud"/>&#160;<xsl:value-of select="caracteristiques/longitud/@unitat"/><br/>
                                Tripulació:<xsl:value-of select="capacitat_humana/tripulacio"/><br/>
                                Tropes: <xsl:value-of select="capacitat_humana/tropes/@quantitat"/> de <xsl:value-of select="capacitat_humana/tropes"/><br/>
                                Propulsió: <xsl:value-of select="propulsio/numero_de_motors_impulsio"/>&#160;<xsl:value-of select="propulsio/motors_impulsio"/><br/>
                            </td> -->
                            <td>
                                <xsl:for-each select="designacions/nom">
                                    <xsl:value-of select="."/><br/>
                                </xsl:for-each>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>