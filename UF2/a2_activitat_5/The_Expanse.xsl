<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <!-- title and ccs file-->
                <title>the Expanse</title>
                <link rel="stylesheet" href="css/estil_SOLUCIO.css" type="text/css"/>
            </head>
            <body>
                <!--t1 titol | logo  -->
                <table border="0" align="center">
                    <tr>
                        <!-- <th><xsl:value-of select="the_expanse/dades/titol"/></th> -->
                        <th>
                            <!-- the expanse logo-->
                            <xsl:element name="img">
                                <xsl:attribute name="src">logos/<xsl:value-of select="the_expanse/dades/logo"></xsl:value-of></xsl:attribute>
                                <xsl:attribute name="alt"><xsl:value-of select="the_expanse/dades/titol"/></xsl:attribute>
                            </xsl:element>
                        </th>
                    </tr>
                </table>
                <!--t2 nom | bandol | caracteristiques | noms coneguts   -->
                <table id="t01" border="1" align="center">
                    <tr>
                        <th width="320">Nom</th>
                        <th width="120">Bandol</th>
                        <th width="400">Característiques</th>
                        <th width="240">Noms coneguts</th>
                    </tr>
                    <!-- Por cada nave -->
                    <xsl:for-each select="the_expanse/Martian_Congressional_Republic_Navy/nau">
                        <tr>
                            <!-- imagen y nombre -->
                            <td class="centrado">
                                <img  width="320" >
                                    <xsl:attribute name="src">imatges/<xsl:value-of select="imatge"/></xsl:attribute>
                                    <xsl:attribute name="alt"><xsl:value-of select="the_expanse/dades/titol"/></xsl:attribute>
                                </img><br/>
                                <a target="_blank" >
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="web"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="classe"/>
                                </a>
                            </td>
                            <!-- Bandol logo MCRN -->
                            <td class="normal">
                                <img  width="120" >
                                    <xsl:attribute name="src">logos/<xsl:value-of select="logo"/></xsl:attribute>
                                    <xsl:attribute name="alt"><xsl:value-of select="the_expanse/dades/titol"/></xsl:attribute>
                                </img>
                            </td>
                            <!-- Característiques -->
                            <td class="normal">
                                Tipus de nau: <xsl:value-of select="tipus"/><br/>
                                Tonelatge: <xsl:value-of select="caracteristiques/tonelatge"/> de <xsl:value-of select="caracteristiques/tonelatge/@unitat"/><br/>
                                Longitud: <xsl:value-of select="caracteristiques/longitud"/>&#160;<xsl:value-of select="caracteristiques/longitud/@unitat"/><br/>
                                Tripulació:<xsl:value-of select="capacitat_humana/tripulacio"/><br/>
                                <!-- Si lleva tropas printea la cantidad en negrita-->
                                <xsl:if test="capacitat_humana/tropes/@quantitat &gt; 0 ">
                                    <b>Tropes: <xsl:value-of select="capacitat_humana/tropes/@quantitat"/> de <xsl:value-of select="capacitat_humana/tropes"/></b><br/>
                                </xsl:if>
                                Propulsió: <xsl:value-of select="propulsio/numero_de_motors_impulsio"/>&#160;<xsl:value-of select="propulsio/motors_impulsio"/><br/>
                            </td>
                            <!-- Noms coneguts -->
                            <td class="normal">
                                <xsl:for-each select="designacions/nom">
                                    <xsl:value-of select="."/><br/>
                                </xsl:for-each>
                                <!-- total de designacions-->
                                (Nº de noms &#61; <xsl:value-of select="count(designacions//nom)"/>)
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>