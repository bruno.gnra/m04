<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <head>

        </head>
        <body>
            <table border="1" align="center">
                <tr>
                    <th colspan="4" align="center" bgcolor="#979A9A"><b>COMANDA</b></th>
                </tr>
                <tr bgcolor="#979A9A">
                    <th align="center">Nom</th>
                    <th align="center">Adreca</th>
                    <th align="center">Ciutat</th>
                    <th align="center">C.P.</th>
                </tr>
                <!--
                <xsl:for-each select="Pedido/Destino">
                    <tr>
                        <td bgcolor="#3366FF"><b><xsl:value-of select="Nombre"/></b></td>
                        <td><xsl:value-of select="Direccion"/></td>
                        <td><xsl:value-of select="Ciudad"/></td>
                        <td align="center"><xsl:value-of select="CodPostal"/></td>
                    </tr>
                </xsl:for-each>
            -->
                <tr>
                    <td align="center" bgcolor="#0174DF"><b><xsl:value-of select="Pedido/Destino/Nombre" /></b></td>
                    <td align="center"><xsl:value-of select="Pedido/Destino/Direccion" /></td>
                    <td align="center"><xsl:value-of select="Pedido/Destino/Ciudad" /></td>
                    <td align="center"><xsl:value-of select="Pedido/Destino/CodPostal" /></td>
                </tr>
                <tr>
                    <td colspan="4">&#160;</td>
                </tr>
                <tr>
                    <td colspan="4" align="center">LLista amb "Precio &gt; 25" i "Precio &lt;&#61; 100"</td>
                </tr>
                <tr>
                    <th colspan="2" align="center">Producte</th>
                    <th align="center">Preu</th>
                    <th align="center">Quantitat</th>
                </tr>
                <xsl:for-each select="Pedido/Contenido/Producto">
                <xsl:sort select="Nombre"/>
                <xsl:if test="Precio &gt; 25 and Precio &lt;&#61; 100">
                    <tr>
                        <xsl:choose>
                            <xsl:when test="Precio &gt; 25 and Precio &lt;50">
                                <td colspan="2">
									<xsl:for-each select="Nombre">
										<xsl:value-of select="." /> (codi = <xsl:value-of select="../@codigo" />)
										<br/>
									</xsl:for-each>
                                </td>
                                <td align="center"><xsl:value-of select="Precio"/></td>
                                <td align="center" bgcolor="F7FE2E"><xsl:value-of select="Cantidad"/></td>
                            </xsl:when>
                            <xsl:when test="Precio &gt; 50 and Precio &lt; 75">
                                <td colspan="2">
                                    <xsl:value-of select="Nombre"/>(codi = <xsl:value-of select="./@codigo"/>)
                                    <br/>
                                </td>
                                <td align="center"><xsl:value-of select="Precio"/></td>
                                <td align="center" bgcolor="#00FF00"><xsl:value-of select="Cantidad"/></td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td colspan="2">
                                    <xsl:value-of select="Nombre"/>(codi = <xsl:value-of select="./@codigo"/>)
                                    <br/>
                                </td>
                                <td align="center"><xsl:value-of select="Precio"/></td>
                                <td align="center" bgcolor="#FF0000"><xsl:value-of select="Cantidad"/></td>
                            </xsl:otherwise>
                        </xsl:choose>
                    </tr>
                </xsl:if>
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>