let $r := doc("submaris.xml")//submarinos
return
<html>
  <head>
    <title>{data($r/titulo)}</title>
    <link rel="stylesheet" type="text/css" href="estil_SOLUCIO.css"/>
  </head>
  <body>
    <table align="center">
    <tr>
      <th>{data($r/titulo)}</th>
    </tr>
    <tr>
      <th><img src="banderes/{data($r/logo)}" style="border-radius: 15px;"/></th>
    </tr>

    </table>
    <table id="t01" align="center" border="1">
      <tr>
        <th width="410" class="normal">Nom</th>
        <th width="410" class="normal">Propulsió</th>
        <th width="220" class="normal">Torpedes</th>
        <th width="250" class="normal">Missils</th>
      </tr>
        {let $submarinos := doc("submaris.xml")//submarinos/submarino
        for $submarino in $submarinos
        
          let $propulsion := $submarino/caracteristicas_generales/propulsion
          let $po_watios := $submarino/caracteristicas_generales/potencia_watios
          let $cv := $submarino/caracteristicas_generales/potencia_caballos
          (:torpedes:)
          let $armas := $submarino/armamento/armas/torpedos/torpedo_identificador/nombre
          (:missils:)
          let $misiles := $submarino/armamento/armas/misiles/misil_identificador/nombre
        return
          <tr>
           
            <td align="center" >
              <img src="imatges/{data($submarino/imatges/foto)}" width="256px"></img><br/>
              <a href="{data($submarino/pag_web)}">{$submarino/nombre}</a>
            </td>
            
            <td align="rigth">
              {$propulsion} de {$po_watios} ({$cv})
            </td>
            
            <td align="rigth">
              {for $t in $armas 
                return <torpedo> {string($t/@cantidad)} del tipus {$t/text()}<br/></torpedo>}
            </td>
           
            <td align="rigth">
              {for $misil in $misiles 
                return <misil> {string($misil/@cantidad)} del tipus {$misil/text()}<br/></misil>}
            </td>
          </tr>}
        
        <tr>
          {let $u_planeadas := sum(doc("submaris.xml")//submarinos/submarino/unidades_planeadas)
           return 
            <td colspan="2" align="rigth"><b>Unitats totals construidas: {$u_planeadas}</b></td>}
  
        {let $u_const := sum(doc("submaris.xml")//submarinos/submarino/unidades_construidas)
          return
            <td colspan="2" align="rigth"><b>Unitats totals plantejadas: {$u_const} </b></td>}
        </tr>
    </table>
  </body>
</html>