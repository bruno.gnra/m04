<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
            </head>
            <body>
                <h2 align="center">Llistat de llibres</h2>
                <table border="1" align="center">
                    <tr>
                        <th align="left">Titulo</th>
                        <th align="left">Editorial</th>
                        <th align="rigth">Precio</th>
                    </tr>
                    <xsl:for-each select="bib/libro">
                        <tr>
                            <td><xsl:value-of select="titulo"/></td>
                            <td><xsl:value-of select="editorial"/></td>
                            <td><xsl:value-of select="precio"/></td>
                        </tr>
                    </xsl:for-each>
                </table>

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>