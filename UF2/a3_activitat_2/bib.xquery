let $activitat2_11a.html := doc("bib.xml")
return 
<html>
  <head/>
  <body>
    <table border="1">
      <tr>
        <td align="left">
          <b>TITOL</b>
        </td>
        <td align="left">
          <b>EDITORIAL</b>
        </td>
        <td align="right">
          <b>PREU</b>
        </td>
      </tr>
      {
      for $i in (doc("bib.xml")/bib/libro)
      return
        <tr>
          <td>
            {data($i/titulo)}
          </td>
          <td>
            {data($i/editorial)}
          </td>
          <td align="right">
            {data($i/precio)}
          </td>
        </tr>
      }
    </table>
  </body>
</html>