<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
            </head>
            <body>

                <div style="width: 40%; height: 20%; border: 1px solid black;">
                    <h1 align="left">Libros de <xsl:value-of select="bib/libro/autor/apellido"/></h1>
                    <table border="1" align="center">
                        <tr>
                            <th>Titulo</th>
                            <th>Precio</th>
                        </tr>
                        <!-- Variable con el apellido del autor indicado-->
                        <xsl:variable name="autor" select="'Stevens'"/>
                        <xsl:for-each select="bib/libro">
                        <xsl:if test="autor/apellido = $autor"> 
                            <tr>
                                <td><xsl:value-of select="titulo"/></td>
                                <td><xsl:value-of select="precio"/></td>
                            </tr>
                        </xsl:if>
                        </xsl:for-each>
                        <tr>
                            <th align="right">Precio total</th>
                            <!-- suma de los precios de cada titulo segun el autor-->
                            <td><xsl:value-of select="sum(bib/libro[autor/apellido = $autor]/precio)"/></td>
                        </tr>
                    </table>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>