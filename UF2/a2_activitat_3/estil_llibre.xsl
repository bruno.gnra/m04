<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <title>Activitat 3 XSLT</title>
            </head>
            <body>
                <table frame="box" style="margin-left:auto;margin-right:auto;" >
                    <xsl:for-each select="bookstore/book">
                    <xsl:sort select="@category" order="descending"/>
                    <xsl:if test="year = 2003 and price &gt;&#61; 30">
                        <tr>
                            <th colspan="7" align="left" bgcolor="#979A9A" ><a style="color:yellow">Categoria:</a><xsl:value-of select="./@category"/></th>
                        </tr>
                        <tr>
                            <td>Titol</td> 
                            <td colspan="3"></td>
                            <td><xsl:value-of select="title"/></td>
                        </tr>
                            <td>Any</td> 
                            <td colspan="3"></td>
                            <td><xsl:value-of select="year"/></td>
                        <tr>
                            <td>Preu</td>
                            <td colspan="3"></td>
                            <td><xsl:value-of select="price"/></td>
                        </tr>
                        <tr>
                            <th colspan="7" align="center" bgcolor="#33FDFF">Autor/s</th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <xsl:for-each select="author">
                                    <xsl:sort select="."/>
                                        <xsl:value-of select="."/><br/>
                                </xsl:for-each>
                            </td>
                        </tr>
                    </xsl:if>
                    </xsl:for-each>   
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
