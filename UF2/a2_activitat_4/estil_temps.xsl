<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <title>El temps</title>
                <!-- Modifica el XSL per a poder fer servir un CSS-->
                <link href="estil_SOLUCIO.css" rel="stylesheet" type="text/css"/> 
            </head>
            <body>
                <!-- titulo -->
                <h2 style="text-align:center;background-color:#979A9A;"><xsl:value-of select="root/origen/productor"/></h2>
                <!-- link AEMET -->
                <p style="text-align:center;">
                    <a target="_blank">
                        <xsl:attribute name="href">
                            <xsl:value-of select="root/origen/web"/>
                        </xsl:attribute>
                        <xsl:value-of select="root/origen/web"/>
                    </a>
                </p>
                <!-- link dades per a Teruel -->
                <p>
                    Dades per a:                  
                    <a target="_blank" style="text-decoration:none;">
                        <xsl:attribute name="href">
                            <xsl:value-of select="root/origen/enlace"/>
                        </xsl:attribute>
                        <xsl:value-of select="root/nombre"/> - <xsl:value-of select="root/provincia"/>
                    </a>
                </p>
                <!-- Tabla de datos-->
                <table border="1" align="center">
                    <xsl:for-each select="root/prediccion/dia">
                    <xsl:sort select="@fecha" order="ascending"/>
                    <!-- Dia -->
                    <tr>
                        <td colspan="3">Data: <a style="color:white; background-color:blue;"><b><xsl:value-of select="@fecha"/></b></a></td>
                    </tr>
                    <!-- Probabilitat Precipitació -->
                    <tr>
                        <th align="center" colspan="3" style="background-color:yellow;">Probabilitat Precipitació</th>
                    </tr>

                    <xsl:for-each select="prob_precipitacion">
                        <tr>
                            <td><xsl:value-of select="@periodo"/></td>
                            <td colspan="2"><xsl:value-of select="."/></td>
                        </tr>
                    </xsl:for-each>
                    <!-- Dades del Vent -->
                    <tr>
                        <th colspan="3" align="center" style="background-color:yellow;">Dades del Vent</th>
                    </tr>
                    <tr style="font-weight:bold; background-color:#979A9A; text-align:center;">
                        <td>Periode</td>
                        <td>Direcció</td>
                        <td>Velocitat</td>
                    </tr>
                    <!-- Datos de Periode|Direcció|Velocitat-->
                    <xsl:for-each select="viento">
                        <tr>
                            <td><xsl:value-of select="@periodo"/></td>
                            <td><xsl:value-of select="direccion"/></td>
                            <!-- Velocitat superior|inferior a ... -->
                            <xsl:choose>
                                <!-- superior a ... -->
                                <xsl:when test="velocidad &gt; 8 ">
                                    <td style="color:red; border: 1px solid red;"><xsl:value-of select="velocidad"/></td>
                                </xsl:when>
                                <!-- inferior a ... -->
                                <xsl:otherwise>
                                    <td style="color:green; border: 1px solid green;"><xsl:value-of select="velocidad"/></td>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tr>
                    </xsl:for-each>
                    <!--  Per a cada dia, la velocitat mitjana del vent-->
                    <tr style="font-weight:bold; background-color:#979A9A; text-align:center;">
                        <td>Quantitat registres vent</td>
                        <td>Suma velocitats</td>
                        <td>Mitjana velocitat</td>
                    </tr>
                    <!-- count | sum | avg (sum/count)-->
                    <tr>
                        <td><xsl:value-of select="count(viento)"/></td>
                        <td><xsl:value-of select="sum(viento/velocidad)"/></td>
                        <td><xsl:value-of select="sum(viento/velocidad) div count(viento)"/></td>
                    </tr>
                    <!-- Temperatura/UV -->
                    <tr>
                        <th colspan="3" align="center" style="background-color:#979A9A;" >Temperatura/UV</th>
                    </tr>
                    <tr style="font-weight:bold; background-color:#979A9A; text-align:center;">
                        <td>Temp.Maxima</td>
                        <td>Temp.Minima</td>
                        <td>UV</td>
                    </tr>
                    <!-- Datos temperatura max|min|uv_max -->
                    <tr>
                        <td><xsl:value-of select="temperatura/maxima"/></td>
                        <td><xsl:value-of select="temperatura/minima"/></td>
                        <td><xsl:value-of select="uv_max"/></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
