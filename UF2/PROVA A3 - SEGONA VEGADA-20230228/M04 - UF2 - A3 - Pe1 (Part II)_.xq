let $i := doc("M04 - UF2 - A3 - Pe1 (Part II)_.xml")//armament
return
<html>
  <head>
  </head>
  <body>
    <table align="center" border="1">
      <tr>
        <th colspan="3">
        <img src="imatges_y_banderes/banderes/{data($i/bandera)}" width="50%"/>
        </th>
      </tr>
      <tr>
        <th colspan="3">Missils aire-aire amb sistema de guia radar</th>
      </tr>
      <tr>
        <th>Nom</th>
        <th>Caracteristiques</th>
        <th>Avions portadors</th>
      </tr>
        {
          
          for $j in ($i/misils_aire_aire/misil_aire_aire)
            let $any := ($j/anys_produccio)
            order by $any
            return
              <tr>
                <td align="center">
                  <a href="{$j/pag_web}"><img src="imatges_y_banderes/imatgesAvions/{data($j/foto)}" width="256"/></a><br/>
                  <a href="{$j/pag_web}">{$j/nom_otan}</a>
                </td>
                <td>
                 Pe: {data($j/pes)}<br/>
                 Velocitat: {data($j/velocitat_mach)}<br/>
                 Abast: {data($j/abast)}<br/>
                 Sistema de guia: {data($j/sistema_de_guia)}<br/>
                 Anys de producció: {data($j/anys_produccio)}<br/>
                </td>
                <td>
                  { let $a := $j//avions/avio
                    for $b in $a
                      return <avion>{$b}</avion>}<br/>
                </td>
              </tr>
        }
        <tr>
          <td>Unitats amb sistema de guia rada: {
            count($i/misils_aire_aire//misil_aire_aire[contains(sistema_de_guia,"radar")])
          } </td>
          <td>Unitats amb altres sistemas de guia:{
            count($i/misils_aire_aire//misil_aire_aire[contains(sistema_de_guia,"radar")])
          } </td>
        </tr>
    </table>
  </body>
</html>