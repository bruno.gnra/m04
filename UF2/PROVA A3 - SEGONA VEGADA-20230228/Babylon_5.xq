let $i := doc("Babylon_5.xml")//nau
return
<html>
  <head>
  </head>
  <body>
  <table align="center" border="1">
    <tr>
      <td>CLASSE</td>
      <td>TRIPULACIO</td>
    </tr>
    {
      for $nau in $i
       let $tripu := ($nau/capacitat_humana/tripulacio)
        return 
          <tr>
                <td style="text-align:center;">{string($nau//@classe)}<br/></td>
                <td align="center">{($tripu)}<br/></td>
          </tr>
    }
    
    <tr>
      <td style="text-align:center;"><b>Total de naus conegudas</b></td>
      <td style="text-align:center;"><b>{sum($i/noms/@coneguts)}</b></td>
    </tr>
  </table>
  </body>
</html>